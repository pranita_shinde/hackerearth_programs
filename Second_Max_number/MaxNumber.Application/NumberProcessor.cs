﻿using System;

namespace MaxNumber.Application
{
    public class NumberProcessor
    {
       public int FindSecondMaxNumber(int[] numbers)
        {
            int temp = 0;
            //Array.Sort(numbers);
            int largest = numbers[0];
            int secondLargest = numbers[0];
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > largest)
                {
                    secondLargest = largest;
                    largest = numbers[i];
                }
                else if ((numbers[i] < largest && numbers[i] > secondLargest) || largest == secondLargest)
                {
                    secondLargest = numbers[i];
                }
            }
            return secondLargest;
        }
    }
}