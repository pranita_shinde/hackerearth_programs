﻿using MaxNumber.Application;
using System;

namespace Second_Max_number
{
    internal  class Program
    {
        static void Main(string[] args)
        {
            int[] NumberOfArray = new int[10];
            Console.WriteLine("Enter the 10 array elements");
            for (int i = 0; i < NumberOfArray.Length; i++)
            {
                NumberOfArray[i] = Convert.ToInt32(Console.ReadLine());
            }
            var numberProcessor = new NumberProcessor();
            var secondMaxNumber = numberProcessor.FindSecondMaxNumber(NumberOfArray);
            Console.WriteLine("Second Max number is= "+secondMaxNumber);
            Console.ReadLine();

        }
    }
}
